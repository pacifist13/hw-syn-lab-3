`timescale 1ns / 1ns
module testSystem();

reg btnC,btnU,clk;
reg [7:0] sw;
wire [6:0] seg;
wire [3:0] an;
wire dp;

system3 s(btnC,btnU,sw,clk,seg,dp,an);

always #5 clk = ~clk;

initial
begin
    #0 clk = 0;btnC=0;btnU=0;sw=7'b00000000;
    #10 btnC = 1;
    #20 btnC = 0;btnU=1;
    #50 btnU = 0; sw[0] = 1;
    #100;
    $finish;
end

endmodule

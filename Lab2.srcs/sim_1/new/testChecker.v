`timescale 1ns / 1ns

module testChecker();

reg [3:0] input1 = 4'b1001;
reg [3:0] input2 = 4'b1001;
reg [3:0] input3 = 4'b1001;
reg [3:0] input4 = 4'b1001;
reg [3:0] check = 4'b1001;
wire e;

digitChecker dc(input1,input2,input3,input4,check,e);

initial
begin
    #0 input3 = 4'b0000;
    #10 input1 = 4'b0000;input2 = 4'b0000;input3 = 4'b0000;input4 = 4'b0000;
    #20 check = 4'b0000;
    #50
    #100;
    $finish;
end

endmodule

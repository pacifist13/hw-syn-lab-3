`timescale 1ns / 1ns

module testSinglePulser();

reg pulse, clk;
singlePulser sp1(pulse, out, clk);

always
#2 clk=~clk;
initial
begin
//$dumpfile("testDFlipFlop.dump");
//$dumpvars(1,D1);
#0 
pulse=0;
clk=0;
#9
pulse=1;
#11
pulse=0;
#13
pulse=1;
#17
pulse=0;
#35
pulse=1;
#61
pulse=0;
#100
$finish;
end

endmodule


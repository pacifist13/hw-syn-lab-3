`timescale 1ns / 1ns

module testSystem3();

reg up, down, set9, set0, clk;
wire [3:0] DCBA;
BCDCounter bc1(DCBA, cout, bout, up, down, set9, set0, clk);

always
#2 clk=~clk;
initial
begin
#0 
clk=0;
up = 1;
down = 0;
set9 = 0;
set0 = 0;
#50
up = 0;
down = 1;
#100
set0 = 1;
#150
set0 = 0;
set9 = 1;
#200
$finish;
end

endmodule
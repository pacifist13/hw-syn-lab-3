`timescale 1ns / 1ns

module inputStabilizer(
        input i,
        input clk,
        output o
    );

wire DFF1Output,DFF0Output;
wire SPin;

clockDiv cd(outClk,clk);
DFlipFlop DFF1(DFF1Output,outClk,i);
DFlipFlop DFF0(DFF0Output,outClk,DFF1Output);

assign SPin = (DFF1Output && ~DFF0Output);

singlePulser SP2(SPin,clk,o);

endmodule

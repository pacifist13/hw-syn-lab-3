`timescale 1ns / 1ns


module digitChecker(
        input [3:0] input1,
        input [3:0] input2,
        input [3:0] input3,
        input [3:0] input4,
        input [3:0] check,
        output wire e
    );
    
assign e = (input1 == check) && (input2 == check) && (input3 == check) && (input4 == check);     

endmodule

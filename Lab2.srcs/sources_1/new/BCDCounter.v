`timescale 1ns / 1ns
module BCDCounter(
    output reg [3:0] DCBA,
    output reg cout,
    output reg bout,
    input up,
    input down,
    input set9,
    input set0,
    input clk
);

reg [3:0] current = 4'b0000;

always @(posedge clk)
begin
    if(set0) 
        begin 
            DCBA = 4'b0000;
            current = DCBA;
            bout = 0;
            cout = 0;
        end
    else if(set9) 
        begin
            DCBA = 4'b1001;
            current = DCBA;
            bout = 0;
            cout = 0;
        end
    else
        begin
            if(up)
            begin
                DCBA = current + 1;
                current = DCBA;
                cout = 0;
                bout = 0;
                if(current == 4'b1010)
                    begin
                        current = 4'b0000;
                        DCBA = current;
                        cout = 1;
                    end
            end
            else if(down)
            begin
                DCBA = current - 1;
                current = DCBA;
                cout = 0;
                bout = 0;
                if(current == 4'b1111)
                    begin
                    current = 4'b1001;
                    bout = 1;
                    DCBA = current;
                    end
            end
            else
            begin
                DCBA = current; 
            end
    end
end

endmodule

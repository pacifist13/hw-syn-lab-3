`timescale 1ns / 1ns

module system3(
    input btnC,
    input btnU,
    input [7:0] sw,
    input clk,
    output [6:0] seg,
    output dp,
    output [3:0] an
    );

wire [3:0] num0;
wire [3:0] num1;
wire [3:0] num2;
wire [3:0] num3;
wire DB3_2Output,DB3_1Output;
wire DB2_2Output,DB2_1Output;
wire DB1_2Output,DB1_1Output;
wire DB0_2Output,DB0_1Output;
wire BTNC,BTNU;

wire [3:0] B3output;
wire B3bout,B3cout,SW7,SW6;
wire [3:0] B2output;
wire B2bout,B2cout,SW5,SW4;
wire [3:0] B1output;
wire B1bout,B1cout,SW3,SW2;
wire [3:0] B0output;
wire B0bout,B0cout,SW1,SW0;

wire targetClk;
wire an0,an1,an2,an3;




wire [18:0] tclk;

assign tclk[0] = clk;

genvar c;
generate for(c=0;c<18;c=c+1)
begin
    clockDiv fdiv(tclk[c+1],tclk[c]);
end endgenerate

clockDiv fdivTarget(targetClk,tclk[18]);

//input processing --> btnC
inputStabilizer isC(btnC,targetClk,BTNC);

//input processing --> btnU
inputStabilizer isU(btnU,targetClk,BTNU);

//input processing --> sw
inputStabilizer isSW0(sw[0],targetClk,SW0);
inputStabilizer isSW1(sw[1],targetClk,SW1);
inputStabilizer isSW2(sw[2],targetClk,SW2);
inputStabilizer isSW3(sw[3],targetClk,SW3);
inputStabilizer isSW4(sw[4],targetClk,SW4);
inputStabilizer isSW5(sw[5],targetClk,SW5);
inputStabilizer isSW6(sw[6],targetClk,SW6);
inputStabilizer isSW7(sw[7],targetClk,SW7);


singlePulser SPB0b(B0bout,targetClk,B0b);
singlePulser SPB0c(B0cout,targetClk,B0c);

singlePulser SPB1b(B1bout,targetClk,B1b);
singlePulser SPB1c(B1cout,targetClk,B1c);

singlePulser SPB2b(B2bout,targetClk,B2b);
singlePulser SPB2c(B2cout,targetClk,B2c);

singlePulser SPB3b(B3bout,targetClk,B3b);
singlePulser SPB3c(B3cout,targetClk,B3c);

BCDCounter B3(B3output,B3cout,B3bout,(SW7 || B2c) && ~a9,(SW6 || B2b) && ~a0,BTNU,BTNC,targetClk);
BCDCounter B2(B2output,B2cout,B2bout,(SW5 || B1c) && ~a9,(SW4 || B1b) && ~a0,BTNU,BTNC,targetClk);
BCDCounter B1(B1output,B1cout,B1bout,(SW3 || B0c) && ~a9,(SW2 || B0b) && ~a0,BTNU,BTNC,targetClk);
BCDCounter B0(B0output,B0cout,B0bout,SW1 && ~a9,SW0 && ~a0,BTNU,BTNC,targetClk);

digitChecker dc9(B0output,B1output,B2output,B3output,4'b1001,a9);
digitChecker dc0(B0output,B1output,B2output,B3output,4'b0000,a0);

quadSevenSeg q7seg(seg,dp,an0,an1,an2,an3,B0output,B1output,B2output,B3output,targetClk);

assign an={an3,an2,an1,an0};

endmodule

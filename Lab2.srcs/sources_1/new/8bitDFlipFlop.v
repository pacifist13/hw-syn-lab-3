`timescale 1ns / 1ns

module eightbitDFlipFlop(
    output [7:0] q,
    output clock,
    input [7:0] d
    );
reg q;
always @(clock)
begin
q=d;
end
endmodule

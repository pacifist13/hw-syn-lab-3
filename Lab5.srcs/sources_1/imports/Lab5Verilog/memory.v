`timescale 1ns / 1ps
//-------------------------------------------------------
// File name    : memory.v
// Title        : Memory
// Library      : nanoLADA
// Purpose      : Computer Architecture
// Developers   : Krerk Piromsopa, Ph. D.
//              : Chulalongkorn University.
module memory(data,address,wr,clock,sw,seg,an,dp);

parameter DATA_WIDTH=32;
parameter ADDR_WIDTH=16;

inout	[DATA_WIDTH-1:0]	data;
input	[ADDR_WIDTH-1:0]	address;
input		wr;
input		clock;
input   [11:0] sw;
output wire [6:0] seg;
output [3:0] an;
output dp;
reg [3:0] num0,num1,num2,num3;
reg [3:0] A,B,OP;
wire dp,an0,an1,an2,an3;
wire targetClk;

//take only 4bit for synthesis
reg	[4:0]	mem[0:1<<ADDR_WIDTH];

reg	[DATA_WIDTH-1:0]	data_out;
// Tri-State buffer
assign data=(wr==0) ? data_out:32'bz;

//quad7seg
wire [18:0] tclk;
assign tclk[0] = clock;
genvar c;
generate for(c=0;c<18;c=c+1)
begin
    clockDiv fdiv(tclk[c+1],tclk[c]);
end endgenerate
clockDiv fdivTarget(targetClk,tclk[18]);

quadSevenSeg q7seg(seg,dp,an,num0,num1,num2,num3,targetClk);

// The following code either initializes the memory values to a specified file or to all zeros to match hardware
generate
    begin: init_bram_to_zero
        integer ram_index;
        initial
        for (ram_index = 0; ram_index < ADDR_WIDTH; ram_index = ram_index + 1)
            mem[ram_index] = 0;
    end
endgenerate

//Mem Mapping for read
always @(address)
begin
            if(address == 16'hFFF0) begin 
                data_out = {28'b0,num0};
            end
            else if (address == 16'hFFF4) begin
                data_out = {28'b0,num1};
            end
            else if (address == 16'hFFF8) begin
                data_out = {28'b0,num2};
            end
            else if (address == 16'hFFFC) begin
                data_out = {28'b0,num3};
            end
            else if (address == 16'hFFE0) begin
                num0 = sw[3:0];
                data_out = {28'b0,num0};
            end
            else if (address == 16'hFFE4) begin
                num1 = sw[7:4];
                data_out = {28'b0,num1};
            end
            else if (address == 16'hFFE8) begin
                OP = sw [11:8];
                data_out = {28'b0,OP};
            end
            else begin
                $display("%10d - mem[%h] -  %h\n",$time, address,data_out);	
                data_out = {28'b0,mem[address]};
            end    

end

//Mem Mapping for write
always @(posedge clock)
begin : MEM_WRITE
	if (wr) begin
	        if(address == 16'hFFF0) begin 
                // num0 = data[3:0];
            end
            else if (address == 16'hFFF4) begin
                // num1 = data[3:0];
            end
            else if (address == 16'hFFF8) begin
                num2 = data[3:0];
            end
            else if (address == 16'hFFFC) begin
                num3 = 4'b0000;
            end
            else begin
		        $display("%10d - MEM[%h] <- %h",$time, address, data);
                mem[address]={28'b0,data[3:0]};
            end   
		
	end
end

endmodule
